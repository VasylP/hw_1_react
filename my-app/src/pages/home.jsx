import { Component } from "react";
import Button from "../components/button";
import Modal from "../components/modal";

export class Home extends Component {
    constructor (props){
        super(props);
          this.state = {isActiveModal1: false, isActiveModal2 : false};

    }
    cheangeActive = (e) => {
        const key = e.target.getAttribute('data-modal-id');
            switch (true) {
                case (key === 'isActiveModal1'):
                    this.setState({ isActiveModal1: !this.state.isActiveModal1 });
                break;
                case (key === 'isActiveModal2'):
                    this.setState({ isActiveModal2: !this.state.isActiveModal2});
                break;
            }
    }
    showModal = (arr) => {
        return arr.map((element) =>
        this.state[element.id] ?
                <Modal 
                    key = {element.id}
                    id = {element.id}
                    windowClassName = {element.id}
                    title = {element.titleText}
                    closeButton = {element.closeButton}
                    text = {element.text}
                    modalBtn = {element.actionsBtn}
                    modalClose = {this.cheangeActive}
                /> : ''
        )
    }
    render () {
        const modalsArray = [
            {
              id:  'isActiveModal1',
              closeButton: true,
              titleText: "Do you want to delete this file?",
              text: 'Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete it?',
              actionsBtn: [
                <Button
                    id = {'isActiveModal1'}
                    modalBtnClassName = {'modal-btn'}
                    bgColor = {'#ea4b35'}
                    color = {'#ffffff'}
                    text = {'Ok'}
                    click = {this.cheangeActive}
                />,
                <Button
                    id = {'isActiveModal1'}
                    modalBtnClassName = {'modal-btn'}
                    bgColor = {'#ea4b35'}
                    color = {'#ffffff'}
                    text = {'Cancel'}
                    click = {this.cheangeActive}
                />
              ]
            },
            {
              id: 'isActiveModal2',
              closeButton: false,
              titleText: "Do you want to save this file?",
              text: 'This file has be save on your computer?',
              actionsBtn: [
                <Button
                    id = {'isActiveModal2'}
                    modalBtnClassName = {'modal-btn'}
                    bgColor = {'#ea4b88'}
                    color = {'#209209'}
                    text = {'Yes'}
                    click = {this.cheangeActive}
                />,
                <Button
                    id = {'isActiveModal2'}
                    modalBtnClassName = {'modal-btn'}
                    bgColor = {'#ea4b88'}
                    color = {'#209209'}
                    text = {'No'}
                    click = {this.cheangeActive}
                />
              ]
            }
          ]
        return (
            <>
            <Button 
                id = 'isActiveModal1'
                bgColor="#03A9F4"
                color="black" 
                text="Modal #1"
                click={this.cheangeActive}
            />
            <Button 
                id = 'isActiveModal2'
                bgColor="#03A9F4"
                color="black" 
                text="Modal #2"
                click={this.cheangeActive}
            />
            {this.showModal(modalsArray)}
            </>
        )
    }
}