import { Component } from "react";
import './button.scss';

class Button extends Component {
    constructor (props) {
        super (props);
    }
    render (){
        return (
            <>
            <button 
                className={`btn ${this.props.btnClassName}`}
                onClick={this.props.click} 
                style={{backgroundColor: `${this.props.bgColor}`, color: `${this.props.color}`}}
                data-modal-id={this.props.id}>
                    {this.props.text}
            </button>
            </>
        )
    }
}

export default Button;